package com.example.administrator.domain;

/**
 * Created by Administrator on 14/03/2018.
 */

public class Client {
    private String codigo;
    private String ruc;
    private String nombre;
    private String apellido;
    private String tipo;
    private Double descuento;

    public Client(String codigo, String ruc, String nombre, String apellido, String tipo, Double descuento) {
        this.codigo = codigo;
        this.ruc = ruc;
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipo = tipo;
        this.descuento = descuento;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getDescuento() {
        return descuento;
    }

    public void setDescuento(Double descuento) {
        this.descuento = descuento;
    }
}
