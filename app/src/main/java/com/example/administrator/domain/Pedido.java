package com.example.administrator.domain;

/**
 * Created by Administrator on 07/08/2018.
 */

public class Pedido {
    private String secuencia, fecha;
    private Double valorTotal;

    public Pedido(String secuencia, String fecha, Double valorTotal) {
        this.secuencia = secuencia;
        this.fecha = fecha;
        this.valorTotal = valorTotal;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }
}
