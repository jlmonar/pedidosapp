package com.example.administrator.domain;

import java.nio.DoubleBuffer;

/**
 * Created by Administrator on 12/03/2018.
 */

public class Item {
    private String name;
    private String quantity;
    private Double price;
    private int numero_item;
    private String codigo;
    private double stock;

    public Item(int numero_item, String name, String quantity, Double price, String codigo, Double stock) {
        this.numero_item = numero_item;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.codigo = codigo;
        this.stock = stock;
    }

    public int getNumero_item() {
        return numero_item;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setNumero_item(int numero_item) {
        this.numero_item = numero_item;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
    }
}
