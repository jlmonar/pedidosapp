package com.example.administrator.config;



import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.administrator.domain.Item;
import android.app.Application;
import android.text.TextUtils;

import java.util.HashMap;

/**
 * Created by José Luis on 13/03/2018.
 */

public class ClaseGlobal  extends Application {
    public static final String TAG = ClaseGlobal.class
            .getSimpleName();

    private HashMap<Integer,Item> mapItems;
    private RequestQueue mRequestQueue;

    private static ClaseGlobal mInstance;

    //Api URL
    //private String API_URL = "http://corporacionsmartest.com/apirestfulpedidosapp/public/api";
    private String API_URL = "http://corporacionsmartest.com/apirestfulpedidosapp_branovi/public/api";
    //private String API_URL = "http://192.168.0.7:8013/apirestfulpedidosapp/public/api";

    //Methods
    private String METHOD_SEARCH_ITEMS_BY_NAME = GET_API_URL() + "/items/searchitems/";
    private String METHOD_FIND_CLIENT_BY_ID = GET_API_URL() + "/clientes/";
    private String METHOD_FIND_TIPO_CLIENTE = GET_API_URL() + "/tipos_clientes/";
    private String METHOD_INSERT_PEDIDO = GET_API_URL() + "/pedido";
    private String METHOD_GET_PEDIDOS_BY_DATE = GET_API_URL() + "/pedido/getbydate/";
    private String METHOD_GET_DETALLES_PEDIDOS = GET_API_URL() + "/detalles_pedido/";
    private String METHOD_GET_PARAMETROS = GET_API_URL() + "/parametros";

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized ClaseGlobal getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public HashMap<Integer, Item> getMapItems() {
        return mapItems;
    }

    public void setMapItems(HashMap<Integer, Item> mapItems) {
        this.mapItems = mapItems;
    }

    public String GET_API_URL() {
        return API_URL;
    }

    public String GET_METHOD_SEARCH_ITEMS_BY_NAME() {
        return METHOD_SEARCH_ITEMS_BY_NAME;
    }

    public String GET_METHOD_FIND_CLIENT_BY_ID() {
        return METHOD_FIND_CLIENT_BY_ID;
    }

    public String GET_METHOD_FIND_TIPO_CLIENTE() {
        return METHOD_FIND_TIPO_CLIENTE;
    }

    public String GET_METHOD_INSERT_PEDIDO() {
        return METHOD_INSERT_PEDIDO;
    }

    public String GET_METHOD_GET_PEDIDOS_BY_DATE() {
        return METHOD_GET_PEDIDOS_BY_DATE;
    }

    public String GET_METHOD_DETALLES_PEDIDO() {
        return METHOD_GET_DETALLES_PEDIDOS;
    }

    public String GET_METHOD_PARAMETROS() {
        return METHOD_GET_PARAMETROS;
    }
}
