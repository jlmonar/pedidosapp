package com.example.administrator.pedidosapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.domain.Client;
import com.example.administrator.domain.Item;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BusquedaItemsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BusquedaItemsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BusquedaItemsFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private SharedPreference sharedPreferences;

    private String nivelPrecioCliente;

    private Button btnSearchProduct;

    private TableLayout itemsTable;

    private ProgressDialog progressDialog;
    private EditText etSearchItem;

    private OnFragmentInteractionListener mListener;

    public BusquedaItemsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BusquedaItemsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BusquedaItemsFragment newInstance(String param1, String param2) {
        BusquedaItemsFragment fragment = new BusquedaItemsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_busqueda_items, container, false);

        sharedPreferences = new SharedPreference();

        nivelPrecioCliente = sharedPreferences.getValue(getContext(), "nivelPrecio", "");

        //Evitar que teclado aparezca automaticamente al iniciar activity.
        btnSearchProduct = (Button) view.findViewById(R.id.btnSearchProduct);
        itemsTable = (TableLayout) view.findViewById(R.id.itemsTable);
        itemsTable.setColumnStretchable(0, true);
        itemsTable.setColumnStretchable(1, true);

        etSearchItem = (EditText) view.findViewById(R.id.etSearchItem);
        btnSearchProduct.setOnClickListener(this);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSearchProduct: {
                //Remover items de busqueda previa
                itemsTable.removeAllViews();

                //Se esconde el teclado
                InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                if (etSearchItem.getText().length() == 0) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Sin resultados.");
                    alert.setMessage("Escriba un texto de búsqueda antes de continuar.");
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    return;
                }

                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Buscando items...");

                String url = ClaseGlobal.getInstance().GET_METHOD_SEARCH_ITEMS_BY_NAME() + etSearchItem.getText();

                // Tag used to cancel the request
                String  tag_string_req = "string_req";

                StringRequest strReq = new StringRequest(Request.Method.GET,
                        url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.toString());
                            /*
                            JSONObject meta = jsonObject.getJSONObject("meta");
                            JSONObject pagination = meta.getJSONObject("pagination");
                            System.out.println("pagination total: " + pagination.getString("total"));
                            System.out.println("pagination total: " + pagination.getString("count"));
                            */

                            int color = 0;
                            JSONArray dataJSONArray = jsonObject.getJSONArray("data");
                            if (dataJSONArray.length() > 0) {
                                for ( int i=0; i < dataJSONArray.length(); i++) {
                                    JSONObject itemJSONObject = dataJSONArray.getJSONObject(i);

                                    Double precioVenta = obtenerPrecioVentaSegunTipoCliente(nivelPrecioCliente, itemJSONObject);

                                    Item item = new Item(itemJSONObject.getInt("identifier"), itemJSONObject.getString("title"),
                                            "0", precioVenta, itemJSONObject.getString("codigo"), itemJSONObject.getDouble("stock"));

                                    color = ((i+1)%2 == 0) ? Color.WHITE: Color.parseColor("#daeaec");

                                    addTableRow(itemsTable, item, color, precioVenta);
                                }
                            } else {
                                AlertDialog.Builder alertError = new AlertDialog.Builder(getActivity());
                                alertError.setTitle("Sin resultados");
                                alertError.setMessage("No se encontraron resultados.");
                                alertError.setPositiveButton("OK",null);
                                alertError.show();
                            }
                        } catch (JSONException e) {
                            AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(getActivity());
                            alertExcepcion.setTitle("Error");
                            alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                            alertExcepcion.setPositiveButton("OK",null);
                            alertExcepcion.show();

                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("RESPONSE API ERROR", "Error: " + error.getMessage());
                        AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(getActivity());
                        alertErrorResponse.setTitle("Error");
                        alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                        alertErrorResponse.setPositiveButton("OK",null);
                        alertErrorResponse.show();
                        progressDialog.dismiss();
                    }
                });
                // Adding request to request queue
                ClaseGlobal.getInstance().addToRequestQueue(strReq, tag_string_req);

                break;
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <progressDialog>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void addTableRow(TableLayout tableLayout, final Item item, int color, double precioVenta) {
        int paddingTop = 40;
        int textSize = 15;

        TableRow tableRow = new TableRow(getActivity().getApplicationContext());
        //tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        tableRow.setBackgroundColor(color);
        tableRow.setClickable(true);

        TableRow.LayoutParams trLayoutParams1;
        trLayoutParams1 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams1.weight = 1; //column weight

        TextView tv1 = new TextView(getActivity().getApplicationContext());
        tv1.setPadding(0,paddingTop,0,paddingTop);
        tv1.setTextColor(Color.BLACK);
        //tv1.setBackgroundDrawable(getResources().getDrawable(R.drawable.table_row_border));
        tv1.setTextSize(textSize);
        tv1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv1.setGravity(Gravity.CENTER);
        tv1.setText(item.getCodigo());
        tv1.setLayoutParams(trLayoutParams1);

        TableRow.LayoutParams trLayoutParams2;
        trLayoutParams2 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams2.weight = (float)1.8;

        TextView tv2 = new TextView(getActivity().getApplicationContext());
        tv2.setPadding(0,paddingTop,0,paddingTop);
        tv2.setTextColor(Color.BLACK);
        //tv2.setBackgroundDrawable(getResources().getDrawable(R.drawable.table_row_border));
        tv2.setTextSize(textSize);
        tv2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv2.setGravity(Gravity.CENTER);
        tv2.setText(item.getName());
        tv2.setLayoutParams(trLayoutParams2);

        TableRow.LayoutParams trLayoutParams3;
        trLayoutParams3 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams3.weight = (float) 0.9;

        TextView tv3 = new TextView(getActivity().getApplicationContext());
        tv3.setPadding(0,paddingTop,0,paddingTop);
        tv3.setTextColor(Color.BLACK);
        //tv2.setBackgroundDrawable(getResources().getDrawable(R.drawable.table_row_border));
        tv3.setTextSize(textSize);
        tv3.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv3.setGravity(Gravity.CENTER);
        tv3.setText("" + precioVenta + " $");
        tv3.setLayoutParams(trLayoutParams3);

        TableRow.LayoutParams trLayoutParams4;
        trLayoutParams4 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams4.weight = (float) 0.9;

        TextView tv4 = new TextView(getActivity().getApplicationContext());
        tv4.setPadding(0,paddingTop,0,paddingTop);
        tv4.setTextColor(Color.BLACK);
        //tv2.setBackgroundDrawable(getResources().getDrawable(R.drawable.table_row_border));
        tv4.setTextSize(textSize);
        tv4.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv4.setGravity(Gravity.CENTER);
        tv4.setText(""+item.getStock());
        tv4.setLayoutParams(trLayoutParams4);

        //ClickListener al tableRow
        tableRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                String jsonItem = gson.toJson(item);
                Intent detailProductActivity = new Intent(getActivity().getApplicationContext(), DetalleProductoActivity.class);
                detailProductActivity.putExtra("item", jsonItem);

                startActivity(detailProductActivity);
            }
        });

        tableRow.addView(tv1);
        tableRow.addView(tv2);
        tableRow.addView(tv3);
        tableRow.addView(tv4);

        tableLayout.addView(tableRow);
    }

    public Double obtenerPrecioVentaSegunTipoCliente(String tipoCliente, JSONObject itemJSONObject) {
        try {
            switch (tipoCliente) {
                case "1": {
                    return itemJSONObject.getDouble("precioDVenta1");
                }
                case "2": {
                    return itemJSONObject.getDouble("precioDVenta2");
                }
                case "3": {
                    return itemJSONObject.getDouble("precioDVenta3");
                }
                case "4": {
                    return itemJSONObject.getDouble("precioDVenta4");
                }
                case "5": {
                    return itemJSONObject.getDouble("precioDVenta5");
                }
                case "6": {
                    return itemJSONObject.getDouble("precioDVenta6");
                }
                case "7": {
                    return itemJSONObject.getDouble("precioDVenta7");
                }
                case "8": {
                    return itemJSONObject.getDouble("precioDVenta8");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
