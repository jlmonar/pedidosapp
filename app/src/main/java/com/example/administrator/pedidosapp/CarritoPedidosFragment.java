package com.example.administrator.pedidosapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.domain.Client;
import com.example.administrator.domain.Item;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CarritoPedidosFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CarritoPedidosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CarritoPedidosFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private LinearLayout llShoppingList, layoutBottomShoppingCart;
    private ClaseGlobal claseGlobal;
    private Context context;
    private SharedPreference sharedPreferences;

    private TextView tvSubtotal, tvIVA, tvTotalCost, tvMessageEmptyCart, tvCliente;
    private ScrollView svListShopingCart;

    private Button btnRealizarPedido;

    private HashMap<Integer,Item> mapItems;

    private ProgressDialog progressDialog;
    private Client cliente;
    private String desglosaIVA;

    private OnFragmentInteractionListener mListener;

    public CarritoPedidosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CarritoPedidosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CarritoPedidosFragment newInstance(String param1, String param2) {
        CarritoPedidosFragment fragment = new CarritoPedidosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        context = getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_carrito_pedidos, container, false);

        llShoppingList = (LinearLayout) view.findViewById(R.id.llShoppingList);
        layoutBottomShoppingCart = (LinearLayout)view.findViewById(R.id.layoutBottomShoppingCart);

        svListShopingCart = (ScrollView) view.findViewById(R.id.svListShopingCart);

        claseGlobal = (ClaseGlobal) context;
        mapItems = claseGlobal.getMapItems();
        sharedPreferences = new SharedPreference();

        desglosaIVA = sharedPreferences.getValue(context, "desglosaIva", "");

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        tvSubtotal = (TextView) view.findViewById(R.id.tvSubtotal);
        tvIVA = (TextView) view.findViewById(R.id.tvIVA);
        tvTotalCost = (TextView) view.findViewById(R.id.tvTotalCost);
        tvMessageEmptyCart = (TextView) view.findViewById(R.id.tvMessageEmptyCart);
        tvCliente = (TextView) view.findViewById(R.id.tvClientePedido);

        btnRealizarPedido = (Button) view.findViewById(R.id.btnRealizarPedido);
        btnRealizarPedido.setOnClickListener(this);

        if (mapItems.size() == 0) {
            tvMessageEmptyCart.setVisibility(View.VISIBLE);
            svListShopingCart.setVisibility(View.GONE);
            layoutBottomShoppingCart.setVisibility(View.GONE);
        } else {
            tvMessageEmptyCart.setVisibility(View.GONE);
            svListShopingCart.setVisibility(View.VISIBLE);
            layoutBottomShoppingCart.setVisibility(View.VISIBLE);
        }

        String jsonClientPref = sharedPreferences.getValue(context, "client", "");
        Gson gson = new Gson();
        cliente = gson.fromJson(jsonClientPref, Client.class);

        tvCliente.setText(cliente.getNombre() + " " + cliente.getApellido());
        double subtotal = 0, IVA = 0;
        double totalCost = 0;
        List<Item> sortedItemsList = sortHashMapItems(mapItems);
        for (Item item : sortedItemsList) {
            //CALCULO DESGLOSANDO IVA
            if (desglosaIVA.equals("S")) {
                subtotal = subtotal + ((Integer.parseInt(item.getQuantity())*item.getPrice()) - item.getPrice() * Integer.parseInt(item.getQuantity()) * (cliente.getDescuento()/100))/1.12;
            } else if(desglosaIVA.equals("N")) {
                //CALCULO SIN DESGLOSAR IVA
                subtotal = subtotal + ((Integer.parseInt(item.getQuantity())*item.getPrice()) - item.getPrice() * Integer.parseInt(item.getQuantity()) * (cliente.getDescuento()/100));
            }

            addCardView(item, cliente);
        }

        DecimalFormat df = new DecimalFormat("#.00");
        IVA = subtotal * 0.12;
        tvSubtotal.setText("$ " + (df.format(subtotal)).toString());
        tvIVA.setText("$ " + df.format(IVA));
        tvTotalCost.setText("$ " + df.format((subtotal + IVA)));

        ////

        ////

        return view;
    }

    public List<Item>  sortHashMapItems(HashMap<Integer, Item> unsortedHashMap) {
        List<Item> list = new ArrayList<>(unsortedHashMap.values());

        //How to sort a List<Object> alphabetically using Object name field
        //https://stackoverflow.com/a/8432848
        if (list.size() > 0) {
            Collections.sort(list, new Comparator<Item>() {
                @Override
                public int compare(final Item object1, final Item object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }

        return list;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRealizarPedido: {
                final AlertDialog.Builder alertPedido = new AlertDialog.Builder(getActivity());
                alertPedido.setTitle("Precaución");
                alertPedido.setMessage("¿Está seguro de finalizar el pedido?");
                alertPedido.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage("Realizando pedido...");

                        String jsonClientPref = sharedPreferences.getValue(context, "client", "");
                        final String jsonItemsPref = sharedPreferences.getValue(context, "items", "");

                        Gson gson = new Gson();
                        final Client clientFromJSON = gson.fromJson(jsonClientPref, Client.class);

                        String urlPedido = ClaseGlobal.getInstance().GET_METHOD_INSERT_PEDIDO();
                        String tag_json_obj = "json_obj_req";
                        StringRequest postRequest = new StringRequest(Request.Method.POST, urlPedido,
                                new Response.Listener<String>()
                                {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response.toString());
                                            String success = jsonObject.getString("data");

                                            if (success.equals("true")) {
                                                claseGlobal.setMapItems(new HashMap<Integer, Item>());
                                                sharedPreferences.removeValue(getActivity(),"items");

                                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                                ft.detach(CarritoPedidosFragment.this).attach(CarritoPedidosFragment.this).commit();

                                                AlertDialog.Builder alertError = new AlertDialog.Builder(getActivity());
                                                alertError.setTitle("Mensaje");
                                                alertError.setMessage("Pedido realizado con éxito.");
                                                alertError.setPositiveButton("OK",null);
                                                alertError.show();
                                            } else {
                                                AlertDialog.Builder alertError = new AlertDialog.Builder(getActivity());
                                                alertError.setTitle("Error");
                                                alertError.setMessage("Error al realizar pedido. Contacte a soporte. ");
                                                alertError.setPositiveButton("OK",null);
                                                alertError.show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            AlertDialog.Builder alertError = new AlertDialog.Builder(getActivity());
                                            alertError.setTitle("Error");
                                            alertError.setMessage("Error al realizar pedido. Contacte a soporte. EXCEPCION");
                                            alertError.setPositiveButton("OK",null);
                                            alertError.show();
                                        }
                                        progressDialog.dismiss();
                                    }
                                },
                                new Response.ErrorListener()
                                {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        // error
                                        AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(getContext());
                                        alertErrorResponse.setTitle("Error");
                                        alertErrorResponse.setMessage("Error al realizar pedido. Contacte a soporte. " + error.getMessage());
                                        alertErrorResponse.setPositiveButton("OK",null);
                                        alertErrorResponse.show();
                                        progressDialog.dismiss();
                                    }
                                }
                        ) {
                            @Override
                            protected Map<String, String> getParams()
                            {
                                Map<String, String>  params = new HashMap<>();
                                String codigo = "";
                                if (clientFromJSON.getCodigo().length() == 10) {
                                    codigo = clientFromJSON.getCodigo();
                                } else if(clientFromJSON.getRuc().length() == 13) {
                                    codigo = clientFromJSON.getRuc();
                                }
                                params.put("cliente", codigo);
                                params.put("items", jsonItemsPref);
                                return params;
                            }
                        };
                        postRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Adding request to request queue
                        ClaseGlobal.getInstance().addToRequestQueue(postRequest, tag_json_obj);
                    }
                });
                alertPedido.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });
                alertPedido.show();
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void addCardView(final Item item, Client cliente) {
        final Fragment currentFragment = this;

        final float scale = getResources().getDisplayMetrics().density;
        int v_10dp = (int) (10 * scale + 0.5f);
        int v_15dp = (int) (15 * scale + 0.5f);
        int v_20dp = (int) (25 * scale + 0.5f);

        DecimalFormat df = new DecimalFormat("#.00");

        // Initialize a new CardView
        CardView card = new CardView(context);

        // Set the CardView layoutParams
        LayoutParams cardLayoutParams = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT
        );
        cardLayoutParams.setMargins(0,v_10dp,0,v_10dp);

        card.setLayoutParams(cardLayoutParams);

        // Set CardView corner radius
        card.setRadius(v_15dp);

        // Set cardView content padding
        card.setContentPadding(v_15dp, v_15dp, v_15dp, v_15dp);

        // Set a background color for CardView
        card.setCardBackgroundColor(getResources().getColor(R.color.colorCardView));

        // Set CardView elevation
        card.setCardElevation(v_10dp);


        //Creating layout with content of the card view
        LinearLayout layoutContent = new LinearLayout(context);
        layoutContent.setOrientation(LinearLayout.VERTICAL);

        LayoutParams layoutContentParams = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);
        layoutContent.setLayoutParams(layoutContentParams);

        //Adding textView with name of the product to the card view
        TextView tvProductName = new TextView(context);
        tvProductName.setText(item.getName());
        tvProductName.setTextColor(Color.parseColor("#383838"));
        tvProductName.setTextSize(25);
        tvProductName.setTypeface(null, Typeface.BOLD);
        LayoutParams tvProductNameParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        tvProductName.setLayoutParams(tvProductNameParams);

        layoutContent.addView(tvProductName);

        //Adding textView with price of the product to the card view
        TextView tvProductPrice = new TextView(context);

        double itemPrice = desglosaIVA.equals("S") ? item.getPrice()/1.12 : item.getPrice();
        tvProductPrice.setText("PVP: " + df.format(itemPrice));
        tvProductPrice.setTextColor(Color.parseColor("#4b4b4b"));
        tvProductPrice.setTextSize(18);
        tvProductPrice.setTypeface(null, Typeface.BOLD_ITALIC);
        LayoutParams tvProductPriceParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        tvProductPrice.setLayoutParams(tvProductPriceParams);

        layoutContent.addView(tvProductPrice);

        //Adding textView with price of the product to the card view
        TextView tvDescuento = new TextView(context);

        double descuento = desglosaIVA.equals("S") ? (item.getPrice() * (cliente.getDescuento()/100) * Integer.parseInt(item.getQuantity()))/1.12 : item.getPrice() * (cliente.getDescuento()/100) * Integer.parseInt(item.getQuantity());
        tvDescuento.setText("Descuento: " + df.format(descuento));
        tvDescuento.setTextColor(Color.parseColor("#4b4b4b"));
        tvDescuento.setTextSize(18);
        tvDescuento.setTypeface(null, Typeface.BOLD_ITALIC);
        LayoutParams tvDescuentoParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        tvDescuento.setLayoutParams(tvDescuentoParams);

        layoutContent.addView(tvDescuento);

        //Adding textView with price of the product to the card view
        TextView tvTotal = new TextView(context);
        double total = (itemPrice * Integer.parseInt(item.getQuantity())) - descuento;
        tvTotal.setText("Total: " + df.format(total));
        tvTotal.setTextColor(Color.parseColor("#4b4b4b"));
        tvTotal.setTextSize(18);
        tvTotal.setTypeface(null, Typeface.BOLD_ITALIC);
        LayoutParams tvTotalParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        tvTotal.setLayoutParams(tvTotalParams);

        layoutContent.addView(tvTotal);

        //Creating Linear layout that contains quantity and delete button
        LinearLayout bottomLayout = new LinearLayout(context);
        bottomLayout.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams bottomLayoutParams = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);
        bottomLayoutParams.setMargins(0,v_15dp,0,0);
        bottomLayout.setLayoutParams(bottomLayoutParams);

        //Adding quantity button
        final Button btnQuantity = new Button(context);
        btnQuantity.setId(item.getNumero_item());
        btnQuantity.setText(item.getQuantity());
        btnQuantity.setBackground(getResources().getDrawable(R.drawable.rounded_button));
        btnQuantity.setPadding(v_10dp,0,v_10dp,0);
        btnQuantity.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
        LayoutParams btnQuantityParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        btnQuantity.setLayoutParams(btnQuantityParams);
        btnQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogQuantity = new AlertDialog.Builder(currentFragment.getActivity());
                alertDialogQuantity.setTitle(item.getName());
                alertDialogQuantity.setMessage("Ingrese cantidad:");
                alertDialogQuantity.setCancelable(false);

                final EditText etQuantity = new EditText(currentFragment.getActivity());
                etQuantity.setInputType(InputType.TYPE_CLASS_NUMBER);
                etQuantity.setText(item.getQuantity());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                etQuantity.setLayoutParams(lp);
                alertDialogQuantity.setView(etQuantity);

                alertDialogQuantity.setPositiveButton("ACTUALIZAR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (etQuantity.getText().toString().equals("0")) {
                                    mapItems.remove(btnQuantity.getId());
                                } else {
                                    Item itemedited = mapItems.get(item.getNumero_item());
                                    mapItems.put(itemedited.getNumero_item(), new Item(itemedited.getNumero_item(), itemedited.getName(), etQuantity.getText().toString(), itemedited.getPrice(), itemedited.getCodigo(), itemedited.getStock()));
                                }

                                //Update sharedPreferences
                                Gson gson = new Gson();
                                String jsonHashMapItems = gson.toJson(mapItems);
                                sharedPreferences.saveString(context,"items",jsonHashMapItems);

                                getFragmentManager().beginTransaction().detach(currentFragment).attach(currentFragment).commit();

                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(etQuantity.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                            }
                        });

                alertDialogQuantity.setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(etQuantity.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                                dialog.cancel();
                            }
                        });

                alertDialogQuantity.show();
            }
        });

        //Adding delete button
        final Button btnDelete = new Button(context);
        btnDelete.setText("ELIMINAR");
        btnDelete.setId(item.getNumero_item());
        btnDelete.setBackground(getResources().getDrawable(R.drawable.rounded_button));
        LayoutParams btnDeleteParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        btnDeleteParams.setMargins(v_20dp,0,0,0);
        btnDelete.setLayoutParams(btnDeleteParams);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertPedido = new AlertDialog.Builder(currentFragment.getActivity());
                alertPedido.setTitle("Precaución");
                alertPedido.setMessage("Va a eliminar el item " + item.getName() + " del carrito de pedidos. ¿Desea continuar?");
                alertPedido.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        mapItems.remove(btnDelete.getId());

                        //Update sharedPreferences
                        Gson gson = new Gson();
                        String jsonHashMapItems = gson.toJson(mapItems);
                        sharedPreferences.saveString(context,"items",jsonHashMapItems);

                        //Refresh the fragment
                        getFragmentManager().beginTransaction().detach(currentFragment).attach(currentFragment).commit();
                    }
                });
                alertPedido.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });
                alertPedido.show();
            }
        });

        bottomLayout.addView(btnQuantity);
        bottomLayout.addView(btnDelete);

        layoutContent.addView(bottomLayout);

        card.addView(layoutContent);

        llShoppingList.addView(card);
    }
}
