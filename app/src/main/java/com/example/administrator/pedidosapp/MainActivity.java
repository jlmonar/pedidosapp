package com.example.administrator.pedidosapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.domain.Client;
import com.example.administrator.domain.Item;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BusquedaItemsFragment.OnFragmentInteractionListener, CarritoPedidosFragment.OnFragmentInteractionListener,
        HistorialPedidosFragment.OnFragmentInteractionListener{

    private ClaseGlobal claseGlobal;
    private SharedPreference sharedPreferences;
    private TextView tvClientName, tvNombreEmpresa;
    private String desglosa_iva, empresa;

    NavigationView mNavigationView;
    View mHeaderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        // NavigationView
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        // NavigationView Header
        mHeaderView =  mNavigationView.getHeaderView(0);

        tvClientName = (TextView)mHeaderView.findViewById(R.id.tvNombreCliente);
        tvNombreEmpresa = (TextView)mHeaderView.findViewById(R.id.tvNombreEmpresa);

        claseGlobal = (ClaseGlobal) getApplicationContext();
        HashMap<Integer,Item> mapItems = new HashMap<Integer,Item>();

        sharedPreferences = new SharedPreference();
        Gson gson = new Gson();

        String jsonClientPref = sharedPreferences.getValue(this, "client", "");
        String jsonItemsPref = sharedPreferences.getValue(this, "items", "");

        if (jsonClientPref.isEmpty()) {
            //There is not a logged user.
            Intent loginActivity = new Intent(this,LoginActivity.class);
            startActivity(loginActivity);
            return;
        }

        desglosa_iva = sharedPreferences.getValue(this, "desglosa_iva", "");
        empresa = sharedPreferences.getValue(this, "empresa", "");

        tvNombreEmpresa.setText(empresa);

        Client clientFromJSON = gson.fromJson(jsonClientPref, Client.class);

        tvClientName.setText(clientFromJSON.getNombre() + " " + clientFromJSON.getApellido());

        if (!jsonItemsPref.isEmpty()) {
            //Shopping cart have elements.
            //From stringJson to HashMap.
            Type typeMap = new TypeToken<HashMap<Integer, Item>>(){}.getType();
            HashMap<Integer,Item> itemsHashMapFromJSON = gson.fromJson(jsonItemsPref, typeMap);

            claseGlobal.setMapItems(itemsHashMapFromJSON);
        } else {
            //Shopping cart is empty.
            claseGlobal.setMapItems(new HashMap<Integer, Item>());
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        //Se selecciona el primer item del NavigationDrawer.
        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //Avoid to return to login screen.
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        boolean fragmentTransaction = false;
        Fragment fragment = null;
        String  title = "";

        if (id == R.id.nav_search) {
            fragment = new BusquedaItemsFragment();
            fragmentTransaction = true;
            title = "Busqueda Producto";
        } else if (id == R.id.nav_cart) {
            fragment = new CarritoPedidosFragment();
            fragmentTransaction = true;
        } else if (id == R.id.nav_history) {
            fragment = new HistorialPedidosFragment();
            fragmentTransaction = true;
        } else if (id == R.id.nav_logout) {
            sharedPreferences.removeValue(this,"client");
            sharedPreferences.removeValue(this,"items");
            sharedPreferences.removeValue(this,"nivelPrecio");
            Intent loginActivity = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(loginActivity);
        } else if (id == R.id.nav_exit) {
            AlertDialog.Builder alertExitApp = new AlertDialog.Builder(this);
            alertExitApp.setTitle("Precaución");
            alertExitApp.setMessage("¿Está seguro que desea cerrar la aplicación?");
            alertExitApp.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Exit app
                    finishAffinity();
                }
            });
            alertExitApp.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            });
            alertExitApp.show();
        }


        if (fragmentTransaction) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_main, fragment)
                    .commit();

            item.setChecked(true);
            //getSupportActionBar().setTitle(title);
            //getActionBar().setTitle(title);
            //getSupportActionBar().setTitle(item.getTitle());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
