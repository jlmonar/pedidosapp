package com.example.administrator.pedidosapp;

import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.administrator.domain.DetallePedido;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class DetallePedidoActivity extends AppCompatActivity {

    private TextView tvNumeroPedido, tvFecha, tvSubtotal, tvIVA, tvTotalVenta;
    private TableLayout detallesPedidoTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pedido);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detalles_pedido);
        toolbar.setTitle("Detalles pedido");

        String jsonDetallesPedido = getIntent().getStringExtra("detalles_pedido");
        String id_pedido = getIntent().getStringExtra("id_pedido");
        String fecha = getIntent().getStringExtra("fecha");
        String valor_total = getIntent().getStringExtra("valor_total");

        tvNumeroPedido = (TextView)findViewById(R.id.tvNumeroPedido);
        tvFecha = (TextView) findViewById(R.id.tvFechaPedido);
        tvSubtotal = (TextView) findViewById(R.id.tvSubtotalPedido);
        tvIVA = (TextView) findViewById(R.id.tvIVAPedido);
        tvTotalVenta = (TextView) findViewById(R.id.tvTotalVentaPedido);

        DecimalFormat df = new DecimalFormat("#.00");
        double subtotal =  Double.parseDouble(valor_total);
        double iva = subtotal * 0.12;
        double total = subtotal + iva;
        tvNumeroPedido.setText(tvNumeroPedido.getText() + id_pedido);
        tvFecha.setText(fecha);
        tvSubtotal.setText("" + df.format(subtotal));
        tvIVA.setText("" + df.format(iva));
        tvTotalVenta.setText("" + df.format(total));

        detallesPedidoTable = (TableLayout) findViewById(R.id.detallesPedidoTable);

        try {
            JSONObject jsonObject = new JSONObject(jsonDetallesPedido);

            int color = 0;
            JSONArray dataJSONArray = jsonObject.getJSONArray("data");
            if (dataJSONArray.length() > 0) {
                for ( int i=0; i < dataJSONArray.length(); i++) {
                    JSONObject itemJSONObject = dataJSONArray.getJSONObject(i);

                    DetallePedido detallePedido = new DetallePedido(itemJSONObject.getString("linea"), itemJSONObject.getString("producto"), itemJSONObject.getInt("cantidad"),
                            itemJSONObject.getDouble("descuento"), itemJSONObject.getDouble("precio_unitario"), itemJSONObject.getDouble("valor_total"));
                    color = ((i+1)%2 == 0) ? Color.WHITE: Color.parseColor("#daeaec");

                    addTableRow(detallesPedidoTable, detallePedido, color);
                }
            }
        } catch (Exception e) {
            AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(this);
            alertExcepcion.setTitle("Error");
            alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
            alertExcepcion.setPositiveButton("OK",null);
            alertExcepcion.show();

            e.printStackTrace();
        }

    }

    public void addTableRow(TableLayout tableLayout, final DetallePedido detallePedido, int color) {
        int paddingTop = 40;
        int textSize = 15;

        TableRow tableRow = new TableRow(getApplicationContext());
        tableRow.setBackgroundColor(color);
        tableRow.setClickable(true);

        TableRow.LayoutParams trLayoutParams1;
        trLayoutParams1 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams1.weight = 2; //column weight

        TextView tv1 = new TextView(getApplicationContext());
        tv1.setPadding(0,paddingTop,0,paddingTop);
        tv1.setTextColor(Color.BLACK);
        tv1.setTextSize(textSize);
        tv1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv1.setGravity(Gravity.CENTER);
        tv1.setText(detallePedido.getProducto());
        tv1.setLayoutParams(trLayoutParams1);

        TableRow.LayoutParams trLayoutParams2;
        trLayoutParams2 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams2.weight = 1;

        TextView tv2 = new TextView(getApplicationContext());
        tv2.setPadding(0,paddingTop,0,paddingTop);
        tv2.setTextColor(Color.BLACK);
        tv2.setTextSize(textSize);
        tv2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv2.setGravity(Gravity.CENTER);
        tv2.setText("" + detallePedido.getCantidad());
        tv2.setLayoutParams(trLayoutParams2);

        TableRow.LayoutParams trLayoutParams3;
        trLayoutParams3 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams3.weight = 1;

        TextView tv3 = new TextView(getApplicationContext());
        tv3.setPadding(0,paddingTop,0,paddingTop);
        tv3.setTextColor(Color.BLACK);
        tv3.setTextSize(textSize);
        tv3.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv3.setGravity(Gravity.CENTER);
        tv3.setText("" + detallePedido.getValor_total());
        tv3.setLayoutParams(trLayoutParams3);

        tableRow.addView(tv1);
        tableRow.addView(tv2);
        tableRow.addView(tv3);

        tableLayout.addView(tableRow);
    }
}
