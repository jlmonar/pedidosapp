package com.example.administrator.pedidosapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.domain.Item;
import com.google.gson.Gson;

import java.util.HashMap;

public class DetalleProductoActivity extends AppCompatActivity implements View.OnClickListener{
    private Item item;
    private TextView tvProductName, tvProductPrice;
    private EditText etProductQuantity;
    private Button btnAddToCart;

    private ClaseGlobal claseGlobal;

    private SharedPreference sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_producto);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Detalles producto");
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        claseGlobal = (ClaseGlobal) getApplicationContext();

        sharedPreferences = new SharedPreference();

        String jsonItem = getIntent().getStringExtra("item");

        Gson gson = new Gson();
        item = gson.fromJson(jsonItem, Item.class);

        tvProductName = (TextView) findViewById(R.id.tvProductName);
        tvProductPrice = (TextView) findViewById(R.id.tvProductPrice);
        etProductQuantity = (EditText) findViewById(R.id.etProductQuantity);

        btnAddToCart = (Button) findViewById(R.id.btnAddToCart);

        tvProductName.setText(item.getName());
        tvProductPrice.setText(item.getPrice().toString());

        btnAddToCart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddToCart: {
                if (etProductQuantity.getText().toString().equals("0") ||
                        etProductQuantity.getText().toString().isEmpty()) {
                    AlertDialog.Builder alertMessage = new AlertDialog.Builder(DetalleProductoActivity.this);
                    alertMessage.setTitle("Precaución");
                    alertMessage.setMessage("Ingrese una cantidad antes de continuar");
                    alertMessage.setPositiveButton("Entendido", null);
                    alertMessage.show();
                } else {
                    HashMap<Integer,Item> mapItems = claseGlobal.getMapItems();
                    Item itemFind = mapItems.get(item.getNumero_item());

                    if (itemFind == null) {
                        System.out.println("No existe item en map, se agregó al carrito");
                        item.setQuantity(etProductQuantity.getText().toString());
                        mapItems.put(item.getNumero_item(), item);
                    } else {
                        System.out.println("Item encontrado: " + itemFind.getName());
                        int newQuantity = Integer.parseInt(itemFind.getQuantity()) + Integer.parseInt(etProductQuantity.getText().toString());
                        itemFind.setQuantity(""+ newQuantity);
                        mapItems.put(itemFind.getNumero_item(),itemFind);
                    }

                    //Refresh items sharedPreferences
                    Gson gson = new Gson();
                    String jsonMapItems = gson.toJson(mapItems);
                    sharedPreferences.saveString(this, "items", jsonMapItems);

                    Toast.makeText(DetalleProductoActivity.this,"Agregado al carrito", Toast.LENGTH_LONG).show();

                    onBackPressed();
                    /*
                    Intent mainActivity = new Intent(DetalleProductoActivity.this, MainActivity.class);
                    startActivity(mainActivity);
                    */
                }
                break;
            }
        }

    }
}
