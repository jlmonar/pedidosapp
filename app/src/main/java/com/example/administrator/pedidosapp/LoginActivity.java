package com.example.administrator.pedidosapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.domain.Client;
import com.example.administrator.domain.Item;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText etClientIdentification;
    private Button btnLogin;
    private Button btnExit;

    private SharedPreference sharedPreferences;
    private Client client;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etClientIdentification = (EditText) findViewById(R.id.etClientIdentification);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnExit = (Button) findViewById(R.id.btnExit);

        sharedPreferences = new SharedPreference();

        btnLogin.setOnClickListener(this);
        btnExit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin: {
                /*
                Client client = new Client("120512221114", "Amy", "Cortez");
                Gson gson = new Gson();
                String jsonClient = gson.toJson(client);

                sharedPreferences.saveString(this,"client", jsonClient);

                Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(mainActivity);
                */
                //Se esconde el teclado
                InputMethodManager inputMethodManager =(InputMethodManager)this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (etClientIdentification.getText().length() == 0) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle("Sin resultados.");
                    alert.setMessage("Escriba un numero de cédula antes de continuar.");
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    return;
                }

                progressDialog = new ProgressDialog(this);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Iniciando sesión...");

                String url = ClaseGlobal.getInstance().GET_METHOD_FIND_CLIENT_BY_ID() + etClientIdentification.getText();

                // Tag used to cancel the request
                String  tag_string_req = "string_req";
                final Context context = this;

                StringRequest strReq = new StringRequest(Request.Method.GET,
                        url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response.toString());

                            JSONObject clientDataJson = jsonObject.getJSONObject("data");

                            if (clientDataJson != null) {
                                client = new Client(clientDataJson.getString("identification"), clientDataJson.getString("rucId"), clientDataJson.getString("name"), clientDataJson.getString("lastName"), clientDataJson.getString("customerType"), clientDataJson.getDouble("dcto"));
                                Gson gson = new Gson();
                                String jsonClient = gson.toJson(client);

                                sharedPreferences.saveString(context,"client", jsonClient);

                                String urlNivelPrecio = ClaseGlobal.getInstance().GET_METHOD_FIND_TIPO_CLIENTE() + client.getTipo();

                                // Tag used to cancel the request
                                String  tag_string_req_nivel_precio = "string_req";

                                StringRequest strReqNivelPrecio = new StringRequest(Request.Method.GET,
                                        urlNivelPrecio, new Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response.toString());

                                            JSONObject tipoClienteDataJson = jsonObject.getJSONObject("data");

                                            if (tipoClienteDataJson != null) {
                                                sharedPreferences.saveString(context,"nivelPrecio", tipoClienteDataJson.getString("priceLevel"));

                                                String urlParametros = ClaseGlobal.getInstance().GET_METHOD_PARAMETROS();

                                                // Tag used to cancel the request
                                                String  tag_string_req_parametros = "string_req";

                                                StringRequest strReqParametros = new StringRequest(Request.Method.GET,
                                                        urlParametros, new Response.Listener<String>() {

                                                    @Override
                                                    public void onResponse(String response) {
                                                        try {
                                                            JSONObject jsonObject = new JSONObject(response.toString());
                                                            JSONArray dataJSONArray = jsonObject.getJSONArray("data");

                                                            if (dataJSONArray.length() > 0) {
                                                                for ( int i=0; i < dataJSONArray.length(); i++) {
                                                                    JSONObject itemJSONObject = dataJSONArray.getJSONObject(i);

                                                                    String id = itemJSONObject.getString("id");
                                                                    if (id.equals("1")) {
                                                                        sharedPreferences.saveString(context,"desglosaIva", itemJSONObject.getString("value"));
                                                                    } else if(id.equals("2")) {
                                                                        sharedPreferences.saveString(context,"empresa", itemJSONObject.getString("value"));
                                                                    }
                                                                }

                                                                Intent mainActivity = new Intent(context, MainActivity.class);
                                                                startActivity(mainActivity);
                                                            } else {
                                                                AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                                                alertError.setTitle("Sin resultados");
                                                                alertError.setMessage("No se encontraron resultados.");
                                                                alertError.setPositiveButton("OK",null);
                                                                alertError.show();
                                                            }
                                                        } catch (JSONException e) {
                                                            AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(context);
                                                            alertExcepcion.setTitle("Error");
                                                            alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                                                            alertExcepcion.setPositiveButton("OK",null);
                                                            alertExcepcion.show();

                                                            e.printStackTrace();
                                                        }
                                                        progressDialog.dismiss();
                                                    }
                                                }, new Response.ErrorListener() {

                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                                                        alertErrorResponse.setTitle("Error");
                                                        alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                                                        alertErrorResponse.setPositiveButton("OK",null);
                                                        alertErrorResponse.show();
                                                        progressDialog.dismiss();
                                                    }
                                                });
                                                // Adding request to request queue
                                                ClaseGlobal.getInstance().addToRequestQueue(strReqParametros, tag_string_req_parametros);
                                            } else {
                                                String errorMessage = jsonObject.getString("error");

                                                AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                                alertError.setTitle("Sin resultados");
                                                alertError.setMessage("No se encontraron resultados. ERROR: " + errorMessage);
                                                alertError.setPositiveButton("OK",null);
                                                alertError.show();
                                            }
                                        } catch (JSONException e) {
                                            AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(context);
                                            alertExcepcion.setTitle("Error");
                                            alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                                            alertExcepcion.setPositiveButton("OK",null);
                                            alertExcepcion.show();

                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                                        alertErrorResponse.setTitle("Error");
                                        alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                                        alertErrorResponse.setPositiveButton("OK",null);
                                        alertErrorResponse.show();
                                        progressDialog.dismiss();
                                    }
                                });
                                // Adding request to request queue
                                ClaseGlobal.getInstance().addToRequestQueue(strReqNivelPrecio, tag_string_req_nivel_precio);
                            } else {
                                String errorMessage = jsonObject.getString("error");

                                AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                alertError.setTitle("Sin resultados");
                                alertError.setMessage("No se encontraron resultados. ERROR: " + errorMessage);
                                alertError.setPositiveButton("OK",null);
                                alertError.show();
                            }
                        } catch (JSONException e) {
                            AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(context);
                            alertExcepcion.setTitle("Error");
                            alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                            alertExcepcion.setPositiveButton("OK",null);
                            alertExcepcion.show();

                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                        alertErrorResponse.setTitle("Error");
                        alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                        alertErrorResponse.setPositiveButton("OK",null);
                        alertErrorResponse.show();
                        progressDialog.dismiss();
                    }
                });
                // Adding request to request queue
                ClaseGlobal.getInstance().addToRequestQueue(strReq, tag_string_req);
                break;
            }
            case R.id.btnExit: {
                AlertDialog.Builder alertExitApp = new AlertDialog.Builder(this);
                alertExitApp.setTitle("Precaución");
                alertExitApp.setMessage("¿Está seguro que desea cerrar la aplicación?");
                alertExitApp.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //Exit app
                        finishAffinity();
                    }
                });
                alertExitApp.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });
                alertExitApp.show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        //Avoid back button.
    }
}
